# Fachpraktikum Algorithms on OpenStreetMap data

## Requirements
This project requires the following libraries. 
It was tested with the versions mentioned below. 
Other versions might work as well.

* cmake version 3.10.2

* gcc 7.4.0

* Qt5.12.3

* libosmium2.15.1

Install libosmium like this:

    sudo add-apt-repository ppa:osmadmins/ppa
    sudo apt-get update
    sudo apt-get install libosmium2-dev

Or follow the instructions described here:

[https://github.com/osmcode/libosmium](https://github.com/osmcode/libosmium)

[https://osmcode.org/libosmium/manual.html](https://osmcode.org/libosmium/manual.html)

How to install Qt5.12.3:

    wget http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
    chmod +x qt-unified-linux-x64-online.run
    ./qt-unified-linux-x64-online.run

## Building

    cd <path to project>
    mkdir build
    cd build
    cmake -Bbuild -DCMAKE_PREFIX_PATH=<path to qt>/Qt/5.12.3/gcc_64/lib/cmake/Qt5 ../src
    make

## Running

```
./fapra <path to pbf>
```

## Usage

* click button **Chosse start on map** and click on map to choose start

* click button **Chosse dest on map** and click on map to choose destination

* click radio buttons to choose mode of transportation and mode of routing

* click button **route me** to calculate and display route from start to destination

* if start and destination are not connected a popup appears

## Problems that might (did) happen
* Failed to find "GL/gl.h" in "/usr/include/libdrm".

   apt install mesa-common-dev

   or anything else, that contains the missing file.

* qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found. [...]

   apt install libqt5\* 

   one of those libraries fixes the problem, i just do not know which one.

## Author

Sarah Sophie Stieß (2963899)(st115875@stud.uni-stuttgart.de)


