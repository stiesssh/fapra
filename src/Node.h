//
// Created by maumau on 08.05.19.
//

#ifndef OSMIUM_COUNT_NODE_H
#define OSMIUM_COUNT_NODE_H
class Node {
public:
    unsigned int id;
    double lat;
    double lon;

    bool mot = false; //true iff motorised vehicle might visit this node
    bool ped = false; //true iff any pedestrian might visit this node.

    Node(unsigned int _id, double _lat, double _lon) : id(_id), lat(_lat), lon(_lon) {}
};
#endif //OSMIUM_COUNT_NODE_H
