//
// Created by maumau on 24.04.19.
//
#include <iostream>
#include <vector>
#include <set>
#include <osmium/io/file.hpp>
#include <osmium/io/any_input.hpp>
#include <osmium/visitor.hpp>

#include <chrono> //time measurement

#include "WayHandler.cpp"
#include "NodeHandler.cpp"
#include "ControlWidget.h"

#include <QtWidgets>
#include <memory>


int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " OSMFILE\n";
        std::exit(1);
    }

    Graph *graph = new Graph;
    //std::unique_ptr<Graph> graph = std::make_unique<Graph>(new Graph);
    try {
        osmium::io::File input_file{argv[1]};

        { //get all osmiumways and make parsingEdges
            osmium::io::Reader reader{input_file, osmium::osm_entity_bits::way};
            WayHandler handler = WayHandler(graph);
            osmium::apply(reader, handler);
            reader.close();
        }
        { //get all osmiumnodes and make nodes
            osmium::io::Reader reader{input_file, osmium::osm_entity_bits::node};
            NodeHandler handler = NodeHandler(graph);
            osmium::apply(reader, handler);
            reader.close();
        }
        //connect nodes and edges
        {
            auto start = std::chrono::high_resolution_clock::now();
            graph->init();
            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
            std::cout << "initialised graph in " << duration.count() << " milliseconds" << std::endl;
        }

        graph->print();


    } catch (const std::exception &e) {
        // All exceptions used by the Osmium library derive from std::exception.
        std::cerr << e.what() << '\n';
        std::exit(1);
    }

    QApplication app(argc, argv);


     //doing gui stuff
    auto window = new ControlWidget(graph);
    window->resize( 1340, 940 );
    window->show();

    return app.exec();
}
