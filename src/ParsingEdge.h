//
// Created by maumau on 08.05.19.
//

#ifndef OSMIUM_COUNT_PARSINGEDGE_H
#define OSMIUM_COUNT_PARSINGEDGE_H
class ParsingEdge {
public:
    unsigned long source; //ID in the nodes array
    unsigned long target; //ID in the nodes array
    short maxSpeed;

    ParsingEdge(unsigned long _source, unsigned long _target, short _maxSpeed) :
            source(_source), target(_target), maxSpeed(_maxSpeed) {}
};
#endif //OSMIUM_COUNT_PARSINGEDGE_H
