//
// Created by maumau on 08.05.19.
//

#include <osmium/handler.hpp>
#include "Graph.h"
#include "Node.h"
#include <osmium/osm/node.hpp>

class NodeHandler : public osmium::handler::Handler {
private:
    Graph* graph;
public:
    NodeHandler(Graph* _graph) : osmium::handler::Handler(), graph(_graph) {}

    // This callback is called by osmium::apply for each node in the data.
    void node(osmium::Node &node) noexcept{
        //germany has about 60 000 000 nodes. thus this fits into an int

        unsigned int nodeId = graph->nodes.size();
        unsigned long osmiumId = node.positive_id();

        if (graph->knownNodes.find(osmiumId) == graph->knownNodes.end()) {
            //node not relevant
            return;
        }

        //add new node, init offset
        graph->nodes.emplace_back(Node(nodeId, node.location().lat(), node.location().lon()));
        //add id mapping
        graph->osmiumToNode.emplace(osmiumId, nodeId);

        //remove from knownNodes
        graph->knownNodes.erase(node.positive_id());
    }
};
