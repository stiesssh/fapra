//
// Created by maumau on 23.05.19.
//

#ifndef FAPRA_CONTROLWIDGET_H
#define FAPRA_CONTROLWIDGET_H


#include <QtWidgets/QWidget>
#include <QLineEdit>
#include <QWebEngineView>

#include "Graph.h"
#include "Node.h"

#include <chrono> //time measurement

class ControlWidget : public QWidget {
Q_OBJECT
private slots:

    void routeme() {
        std::cout << "clicked route me" << std::endl;

        if (lineEdit_slon->text().length() == 0 || lineEdit_slat->text().length() == 0) {
            QString code = QStringLiteral("alert( 'Please choose start!' );");
            view->page()->runJavaScript(code);
            return;
        }
        if (lineEdit_dlon->text().length() == 0 || lineEdit_dlat->text().length() == 0) {
            QString code = QStringLiteral("alert( 'Please choose destination!' );");
            view->page()->runJavaScript(code);
            return;
        }
        unsigned int start = graph->nearestNeighbour(slat, slon, motorised).id;
        unsigned int dest = graph->nearestNeighbour(dlat, dlon, motorised).id;

        {
            std::cout << "start dijkstra" << std::endl;
            auto t0 = std::chrono::high_resolution_clock::now();
            graph->dijkstra(start, dest, motorised, shortest, anythingToggld);
            auto t1 = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
            std::cout << "dijkstra finished in " << duration.count() << " milliseconds" << std::endl;
        }
        {
            auto t0 = std::chrono::high_resolution_clock::now();
            paint2();
            auto t1 = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
            std::cout << "painted route in " << duration.count() << " milliseconds" << std::endl;
        }
    }

    void shortestToggled(bool checked) {
        shortest = checked;
        anythingToggld = true;
    }

    void motorisedToggled(bool checked) {
        motorised = checked;
        anythingToggld = true;

        // recalculate nearest neighbour
        {
            Node n = graph->nearestNeighbour(dlat, dlon, motorised);
            view->page()->runJavaScript(QStringLiteral("destMarker.remove()"));
            //set line edit
            lineEdit_dlon->setText(QString::number(n.lon));
            lineEdit_dlat->setText(QString::number(n.lat));
            dlon = n.lon;
            dlat = n.lat;
            QString code = QStringLiteral("destMarker = L.marker([");
            code.append(QString::number(dlat)).append(QStringLiteral(", ")).append(QString::number(dlon)).append(
                    QStringLiteral("]); destMarker.addTo(mymap)"));
            view->page()->runJavaScript(code);
        }
        {
            Node n = graph->nearestNeighbour(slat, slon, motorised);
            view->page()->runJavaScript(QStringLiteral("startMarker.remove()"));
            //set line edit
            lineEdit_slon->setText(QString::number(n.lon));
            lineEdit_slat->setText(QString::number(n.lat));
            slon = n.lon;
            slat = n.lat;
            QString code = QStringLiteral("startMarker = L.marker([");
            code.append(QString::number(slat)).append(QStringLiteral(", ")).append(QString::number(slon)).append(
                    QStringLiteral("]); startMarker.addTo(mymap)"));
            view->page()->runJavaScript(code);
        }
    }

public slots:
    void getlatlon(const QString &jslat, const QString &jslng){

        // remove line of previous routing, if present. causes an 'js: Uncaught ReferenceError: polyline is not defined'
        // if no previous line exists, but works anyway
        view->page()->runJavaScript(QStringLiteral("polyline.remove();"));

        auto t0 = std::chrono::high_resolution_clock::now();
        Node n  = graph->nearestNeighbour(jslat.toDouble(), jslng.toDouble(), motorised);
        auto t1 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        std::cout << "nearest neighbour calculation : " << duration.count() << " milliseconds" << std::endl;

        if (choosing == mode::d) {
            view->page()->runJavaScript(QStringLiteral("destMarker.remove()"));
            //set line edit d
            lineEdit_dlon->setText(QString::number(n.lon));
            lineEdit_dlat->setText(QString::number(n.lat));
            dlon = n.lon;
            dlat = n.lat;
            QString code = QStringLiteral("destMarker = L.marker([");
            code.append(QString::number(dlat)).append(QStringLiteral(", ")).append(QString::number(dlon)).append(QStringLiteral("]); destMarker.addTo(mymap)"));
            view->page()->runJavaScript(code);
        } else if (choosing == mode::s) {
            view->page()->runJavaScript(QStringLiteral("startMarker.remove()"));
            //set line edit s
            lineEdit_slon->setText(QString::number(n.lon));
            lineEdit_slat->setText(QString::number(n.lat));
            slon = n.lon;
            slat = n.lat;
            QString code = QStringLiteral("startMarker = L.marker([");
            code.append(QString::number(slat)).append(QStringLiteral(", ")).append(QString::number(slon)).append(QStringLiteral("]); startMarker.addTo(mymap)"));
            view->page()->runJavaScript(code);

        }
        // reset choosing mode
        choosing = mode::none;
    };

    void choosingS() {
        choosing = mode::s;
    }

    void choosingD() {
        choosing = mode::d;
    }

private:
    bool shortest = true;
    bool motorised = true;
    bool anythingToggld = false;

    enum mode {
        s, d, none
    };
    mode choosing = mode::none;


    double slon = 0.0;
    double slat = 0.0;
    double dlon = 0.0;
    double dlat = 0.0;

    QLineEdit *lineEdit_slat;
    QLineEdit *lineEdit_slon;
    QLineEdit *lineEdit_dlat;
    QLineEdit *lineEdit_dlon;

    QWebEngineView * view;

    Graph *graph;

public:
    explicit ControlWidget(Graph *_graph);

    /**
     * paint poly line as one. its a freaking long string, but its a lot faster that piecewise.
     */
    void paint2() {
        //this causes an 'js: Uncaught ReferenceError: polyline is not defined' but it works anyway
        view->page()->runJavaScript(QStringLiteral("polyline.remove();"));

        // construct new polyline
        // predecessor : that's where the node come from
        unsigned int start = graph->currentStartId;
        unsigned int dest = graph->currentDestId;

        QString code = QStringLiteral("var latlngs = [\n");

        while (start != dest){
            Node from = graph->nodes.at(dest);
            QString coord =  QStringLiteral("[");
            coord.append(QString::number(from.lat));
            coord.append( QStringLiteral(", "));
            coord.append(QString::number(from.lon));
            coord.append( QStringLiteral("],\n"));
            code.append(coord);
            assert(dest != graph->predecessor.at(dest)); //no loops
            dest = graph->predecessor.at(dest);
            if (dest == graph->maxuint) { //maxuint means that there is no predecessor
                view->page()->runJavaScript(QStringLiteral("alert( 'start and dest are not connected. try again' );"));
                return;
            }
        }
        Node from = graph->nodes.at(dest);
        code.append(QStringLiteral("["));
        code.append(QString::number(from.lat));
        code.append(QStringLiteral(", "));
        code.append(QString::number(from.lon));
        code.append(QStringLiteral("]\n"));

        code.append(QStringLiteral("];\n"
                                   "var polyline = L.polyline(latlngs, {color: 'red'}).addTo(mymap);\n"
                                   "// zoom the map to the polyline\n"
                                   "mymap.fitBounds(polyline.getBounds());"));
        view->page()->runJavaScript(code);
    }
};

#endif //FAPRA_CONTROLWIDGET_H
