//
// Created by maumau on 19.06.19.
//

#ifndef FAPRA_PEDEDGE_H
#define FAPRA_PEDEDGE_H

/**
 * PedEdges are Edges for Pedestrians.
 * As Pedestrians travel at constant speed, the traveltime need not be saved.
 */
class PedEdge {
public:
    unsigned int source; //ID in the nodes array
    unsigned int target; //ID in the nodes array
    int distance;

    PedEdge(unsigned int _source, unsigned int _target, int _distance) :
            source(_source), target(_target), distance(_distance) {}


};


#endif //FAPRA_PEDEDGE_H
