//
// Created by maumau on 23.05.19.
//

#include "ControlWidget.h"

#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>

#include <QWebEngineView>
#include <QWebChannel>
#include <QtWidgets/QWidget>

ControlWidget::ControlWidget(Graph * _graph) : QWidget(), graph(_graph) {
    // ???
    setAttribute(Qt::WA_DeleteOnClose, true);

    //load Map
    view = new QWebEngineView();
    view->load(QUrl("qrc:/html/map.html"));

    //connect with webchannel
    auto channel = new QWebChannel(this);
    view->page()->setWebChannel(channel);
    channel->registerObject(QStringLiteral("webobj"), this);

    //make control panel
    auto label_s = new QLabel("Start :");
    auto label_d = new QLabel("Destination :");

    auto label_slat = new QLabel("Lat :");
    auto label_slon = new QLabel("Lon :");
    auto label_dlat = new QLabel("Lat :");
    auto label_dlon = new QLabel("Lon :");

    lineEdit_slat = new QLineEdit();
    lineEdit_slon = new QLineEdit();
    lineEdit_dlat = new QLineEdit();
    lineEdit_dlon = new QLineEdit();

    lineEdit_slat->setReadOnly(true);
    lineEdit_slon->setReadOnly(true);
    lineEdit_dlat->setReadOnly(true);
    lineEdit_dlon->setReadOnly(true);

    auto button_s = new QPushButton("Choose start on map");
    auto button_d = new QPushButton("Choose dest on map");

    /* Mode of Routing */
    auto routing_mode = new QGroupBox("Mode of Routing");
    auto radioRouting1 = new QRadioButton("shortest route");
    radioRouting1->toggle();
    auto radioRouting2 = new QRadioButton("fastest route");

    {
        auto *vbox = new QVBoxLayout;
        vbox->addWidget(radioRouting1);
        vbox->addWidget(radioRouting2);
        vbox->addStretch(1);
        routing_mode->setLayout(vbox);
    }

    /* Mode of Transportation */
    auto transport_mode = new QGroupBox("Mode of Transportation");
    auto radioTransport1 = new QRadioButton("motorised vehicle");
    radioTransport1->toggle();
    auto radioTransport2 = new QRadioButton("pedestrian");

    {
        auto *vbox = new QVBoxLayout;
        vbox->addWidget(radioTransport1);
        vbox->addWidget(radioTransport2);
        vbox->addStretch(1);
        transport_mode->setLayout(vbox);
    }

    auto button = new QPushButton("Route me");

    auto placeholder = new QWidget();
    placeholder->setMinimumHeight(400);

    auto layout = new QGridLayout;

    /* labels and lines for start */
    layout->addWidget(label_s, 0, 0, 1, 2);
    layout->addWidget(label_slat, 1, 0);
    layout->addWidget(lineEdit_slat, 1, 1);
    layout->addWidget(label_slon, 2, 0);
    layout->addWidget(lineEdit_slon, 2, 1);
    layout->addWidget(button_s, 3, 0, 1, 2);

    /* labels and lines for destination */
    layout->addWidget(label_d, 4, 0, 1, 2);
    layout->addWidget(label_dlat, 5, 0);
    layout->addWidget(lineEdit_dlat, 5, 1);
    layout->addWidget(label_dlon, 6, 0);
    layout->addWidget(lineEdit_dlon, 6, 1);
    layout->addWidget(button_d, 7, 0, 1, 2);

    /* checkbox for whom to route*/
    layout->addWidget(routing_mode, 8, 0, 1, 2);
    layout->addWidget(transport_mode, 9, 0, 1, 2);

    /* button to start route calculation */
    layout->addWidget(button, 10, 0, 1, 2);

    /* placeholder */
    layout->addWidget(placeholder, 11, 0, 1, 2);

    /* connect routeme button */
    connect(button, SIGNAL (released()), this, SLOT (routeme()));

    /* connect radiobuttons */
    connect(radioRouting1, SIGNAL (toggled(bool)), this, SLOT (shortestToggled(bool)));
    connect(radioTransport1, SIGNAL (toggled(bool)), this, SLOT (motorisedToggled(bool)));

    /* connect choose buttons */
    connect(button_s, SIGNAL(released()), this, SLOT(choosingS()));
    connect(button_d, SIGNAL(released()), this, SLOT(choosingD()));

    //ugly hierarchy but it works
    auto widgetwidget = new QWidget();
    widgetwidget->setLayout(layout);
    widgetwidget->setMaximumWidth(300);


    auto hlayout = new QHBoxLayout();

    hlayout->addWidget(widgetwidget);
    hlayout->addWidget(view);

    this->setLayout(hlayout);
}