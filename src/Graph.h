//
// Created by maumau on 08.05.19.
//

#ifndef OSMIUM_COUNT_GRAPH_H
#define OSMIUM_COUNT_GRAPH_H

#include <cassert>

#include <queue>
#include <vector>
#include <set>
#include <unordered_map>
#include <algorithm> //for sort
#include <iostream>
#include <cmath> //for pi, sin, cos, hypot
#include <limits>
#include <stdexcept>


#include "Node.h"
#include "ParsingEdge.h"
#include "Edge.h"
#include "PedEdge.h"
#include "DijkstraNode.h"

class Graph {
public:
    // handy constants
    const long maxlong = std::numeric_limits<long>::max();
    const int maxint = std::numeric_limits<int>::max();
    const unsigned int maxuint = std::numeric_limits<unsigned int>::max();

    // the nodes
    std::vector<Node> nodes = std::vector<Node>();

    //the heap and other data structures for dijkstra
    std::priority_queue<DijkstraNode> heap = std::priority_queue<DijkstraNode>();
    std::vector<unsigned int> predecessor = std::vector<unsigned int>();
    std::vector<long> currentmin = std::vector<long>();

    //edges for motorised vehicles
    std::vector<ParsingEdge> motorwayParsingEdges = std::vector<ParsingEdge>(); //deleted after parsing
    std::vector<Edge> motEdges = std::vector<Edge>();
    std::vector<unsigned int> motorwayOffsets = std::vector<unsigned int>();

    //edges for pedestrians
    std::vector<ParsingEdge> pedestrianParsingEdges = std::vector<ParsingEdge>(); //deleted after parsing
    std::vector<PedEdge> pedEdges = std::vector<PedEdge>();
    std::vector<unsigned int> pedestrianOffsets = std::vector<unsigned int>();

    //helper data structures during parsing
    std::set<unsigned long> knownNodes = std::set<unsigned long>();
    std::unordered_map<unsigned long, unsigned int> osmiumToNode = std::unordered_map<unsigned long, unsigned int>();

    //to remember the last executed dijkstra
    unsigned int currentStartId = 0;
    unsigned int currentDestId = 0;

    explicit Graph() {};

    void print() {
        std::cout << "nodes : " << nodes.size() << std::endl;
        std::cout << "motorway edges : " << motEdges.size() << std::endl;
        std::cout << "pedestrian edges : " << pedEdges.size() << std::endl;
    }

    void init() {
        connectMotorways();
        connectPedestrians();
        std::cout << "finished connecting" << std::endl;

        //everything is mapped, we don't need this anymore
        osmiumToNode.clear();
        //resize dijkstra data structure. this takes a lot of time
        currentmin.resize(nodes.size(), maxuint);
        predecessor.resize(nodes.size(), 0);
        std::cout << "finished preparations for dijkstra" << std::endl;

    }

    void connectMotorways() {
        for (unsigned long i = motorwayParsingEdges.size(); i > 0; i--) {
            ParsingEdge* e = &motorwayParsingEdges.at(i-1);

            // because parsing edges reference the original osmium nodes
            unsigned int sourceId = osmiumToNode.at(e->source);
            unsigned int targetId = osmiumToNode.at(e->target);
            // enable nodes as motorway nodes.
            nodes[sourceId].mot = true;
            nodes[targetId].mot = true;


            //calc costs
            long d = distance(nodes.at(sourceId).lat, nodes.at(sourceId).lon, nodes.at(targetId).lat,
                                  nodes.at(targetId).lon);
            long tt = d * e->maxSpeed;

            assert (d < maxint); // check whether distance overflows int
            assert (tt < maxint); // check whether traveltime overflows int

            //update edges
            motEdges.emplace_back(Edge(sourceId, targetId, (int) d, (int) tt));
            //remove handled edge from parsing structure
            motorwayParsingEdges.pop_back();
           }

        //sort motorway edges by source node
        struct EdgeComparator {
            bool operator()(Edge& a, Edge& b) const { //& for reference
                return a.source < b.source;
            }
        };
        std::sort(motEdges.begin(), motEdges.end(), EdgeComparator());

        //init offset array
        motorwayOffsets.resize(nodes.size()+1, motEdges.size()-1);

        //set offset
        unsigned long nextSourceId = 0;
        motorwayOffsets.at(0) = 0;
        for (unsigned int i = 0; i < motEdges.size(); i++) {
            unsigned long sourceId = motEdges.at(i).source;
            //set offset and increase
            assert (sourceId < nodes.size() && sourceId < motorwayOffsets.size()); //source id is not too high to set offset
            while (sourceId > nextSourceId) {
                //sourceId has increased, edge is first edge of new block
                //update
                motorwayOffsets.at(++nextSourceId) = i;
            }
        }
    }


    void connectPedestrians() {
        for (unsigned long i = pedestrianParsingEdges.size(); i > 0; i--) {
            ParsingEdge* e = &pedestrianParsingEdges.at(i-1);

            // because parsing edges reference the original osmium nodes
            unsigned int sourceId = osmiumToNode.at(e->source);
            unsigned int targetId = osmiumToNode.at(e->target);
            // enable nodes as pedestrian nodes.
            nodes[sourceId].ped = true;
            nodes[targetId].ped = true;


            //calc costs
            long d = distance(nodes.at(sourceId).lat, nodes.at(sourceId).lon, nodes.at(targetId).lat,
                              nodes.at(targetId).lon);

            assert (d < maxint); // check whether distance overflows int

            //update edges
            pedEdges.emplace_back(PedEdge(sourceId, targetId, (int) d));
            //remove handled edge from parsing structure
            pedestrianParsingEdges.pop_back();
        }

        //sort pedestrian edges by source node
        struct EdgeComparator {
            bool operator()(PedEdge& a, PedEdge& b) const {
                return a.source < b.source;
            }
        };
        std::sort(pedEdges.begin(), pedEdges.end(), EdgeComparator());

        //init offset array
        pedestrianOffsets.resize(nodes.size()+1, pedEdges.size()-1);
        //set offset
        unsigned long nextSourceId = 0;
        pedestrianOffsets.at(0) = 0;
        for (unsigned int i = 0; i < pedEdges.size(); i++) {
            unsigned long sourceId = pedEdges.at(i).source;
            //set offset and increase
            assert (sourceId < nodes.size() && sourceId < pedestrianOffsets.size()); //source id is not too high to set offset
            while (sourceId > nextSourceId) {
                //sourceId has increased, edge is first edge of new block
                //update
                pedestrianOffsets.at(++nextSourceId) = i;
            }
        }
    }

    /**
     * calculates the distance between two points in meters
     * @param lat1 latitude of the first point
     * @param lon1 longitude of the first point
     * @param lat2 latitude of the second point
     * @param lon2 longitude of the second point
     * @return distance in meters
     */
    long distance(double lat1, double lon1, double lat2, double lon2) {
        const double earth = 6371009; // radius of earth in meter
        const double pi = acos(-1);
        //calculate radians
        double rlat1 = lat1 * (pi / 180.0);
        double rlat2 = lat2 * (pi / 180.0);
        //calculate radian distances
        double rdlat = (lat1 - lat2) * (pi / 180.0);
        double rdlon = (lon1 - lon2) * (pi / 180.0);

        //sign must not be consider because the sinuses are quadrated and the cosines are symmetric to the axis.
        double a = std::sin(rdlat / 2.0) * std::sin(rdlat / 2.0) +
                   std::cos(rlat1) * std::cos(rlat2) * std::sin(rdlon / 2.0) * std::sin(rdlon / 2.0);

        double c = 2.0 * std::atan2(std::sqrt(a), std::sqrt(1 - a));

        return lround(earth * c);
    }

    Node nearestNeighbour(double lat, double lon, bool motorised) {
        Node nearest = nodes.at(0);
        double d = std::sqrt((nearest.lat - lat)*(nearest.lat - lat)+(nearest.lon - lon)*(nearest.lon - lon));
        if (motorised) {
            for (Node& n : nodes) {
                if (n.mot) {
                    double dd =  std::sqrt((n.lat - lat)*(n.lat - lat)+(n.lon - lon)*(n.lon - lon));
                    if (dd < d) {
                        d = dd;
                        nearest = n;
                    }
                }
            }
            return nearest;
        } else {
            for (Node& n : nodes) {
                if (n.ped) {
                    double dd = std::sqrt((n.lat - lat) * (n.lat - lat) + (n.lon - lon) * (n.lon - lon));
                    if (dd < d) {
                        d = dd;
                        nearest = n;
                    }
                }

            }
            return nearest;
        }
    }

    /**
     *
     * @param startid   id of start node
     * @param destinationid     id of destination node
     * @param motorised         true, iff route should be calculated for motorised vehicles, false for pedestrians
     * @param shortest          true, iff shortest route should be calculated, false for fastes
     * @param enforceRecalc     true, if solution of previous dijkstra cannot be reused
     */
    void dijkstra (unsigned int startid, unsigned int destinationid, bool motorised, bool shortest, bool enforceRecalc) {

        // we already calculated for this start node and nothing but the destination node has not changed
        if (currentStartId == startid && !enforceRecalc) {
            currentDestId = destinationid;
            return;
        }

        // to remember
        currentStartId = startid;
        currentDestId = destinationid;

        //reset - resize happens at the end of connect
        //set predecessor to maxuint to recognize if predecessor was not set!
        assert(nodes.size() < maxuint);

        for (unsigned int i = 0; i < nodes.size(); i++) {
            currentmin[i] = maxlong;
            predecessor[i] = maxuint;
        }

        //zero for start node
        heap.push(DijkstraNode(startid, 0));
        currentmin[startid] = 0;

        //up to here it's the same. now it differs depending on the mode of routing and transportation
        // TODO improve maintainability (don not copy code three times, that's ugly :/)
        /*
         * Dijkstra for motorised vehicles and shortest route
         */
        if (motorised && shortest) {
            while (!heap.empty()) {
                //take min
                long minCost = heap.top().cost;
                unsigned int minId = heap.top().id;

                heap.pop();

                if (minCost > currentmin[minId]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = motorwayOffsets[minId]; i < motorwayOffsets[minId+1]; i++){
                    unsigned int succId = motEdges[i].target;
                    long cost = minCost + motEdges[i].distance;
                    assert (cost >= minCost); //check for cost overflow
                    if (currentmin[succId] > cost){
                        currentmin[succId] = cost;
                        predecessor[succId] = minId;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        }
        /*
         * Dijkstra for motorised vehicles and fastest route
         */
        else if (motorised) {
            while (!heap.empty()) {
                //take min
                long minCost = heap.top().cost;
                unsigned int minId = heap.top().id;

                heap.pop();

                if (minCost > currentmin[minId]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = motorwayOffsets[minId]; i < motorwayOffsets[minId+1]; i++){
                    unsigned int succId = motEdges[i].target;
                    long cost = minCost + motEdges[i].traveltime;
                    assert (cost >= minCost); //check for cost overflow
                    if (currentmin[succId] > cost){
                        currentmin[succId] = cost;
                        predecessor[succId] = minId;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        }
        /*
         * Dijkstra for pedestrians
         */
        else {
            while (!heap.empty()) {
                //take min
                long minCost = heap.top().cost;
                unsigned int minId = heap.top().id;

                heap.pop();

                if (minCost > currentmin[minId]) {
                    continue;
                }
                //over all successors
                for (unsigned int i = pedestrianOffsets[minId]; i < pedestrianOffsets[minId+1]; i++){
                    unsigned int succId = pedEdges[i].target;
                    long cost = minCost + pedEdges[i].distance;
                    assert (cost >= minCost); //check for cost overflow
                    if (currentmin[succId] > cost){
                        currentmin[succId] = cost;
                        predecessor[succId] = minId;
                        heap.push(DijkstraNode(succId, cost));
                    }
                }
            }
        }
    }
};

#endif //OSMIUM_COUNT_GRAPH_H
