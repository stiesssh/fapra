//
// Created by maumau on 26.08.19.
//

#ifndef FAPRA_VALIDATER_H
#define FAPRA_VALIDATER_H


#include "Graph.h"

class Validater {

    Validater(Graph* graph) : graph(graph) {}


    void validate() {
        validateAfterRead();
        validateNodesAfterConnect();
        validateMotorwayEdgesAfterConnect();
        validatePedestrianEdgesAfterConnect();
        validateMotorwayOffsetsAfterConnect();
        validatePedestrianOffsetsAfterConnect();
    }

    void validateAfterRead() {
        if (graph->nodes.empty())
            throw std::runtime_error("nodes array is empty");
        if (!graph->motorwayParsingEdges.empty())
            throw std::runtime_error("motorwayParsingEdges array is not empty");
        if (!graph->pedestrianParsingEdges.empty())
            throw std::runtime_error("pedestrianParsingEdges array is not empty");
        if (graph->motEdges.empty())
            throw std::runtime_error("motEdges array is empty");
    }

    void validateMotorwayEdgesAfterConnect() {

        unsigned long previousSourceId = 0;
        for (Edge e : graph->motEdges) {
            // source and target ids of valid nodes
            if (e.source >= graph->nodes.size())
                throw std::runtime_error("motorway edge's source node is not valid");
            if (e.target >= graph->nodes.size())
                throw std::runtime_error("motorway edge's target node is not valid");
            // distance is valid
            if (e.distance < 0 ) //TODO smaller than max distance (DE vertical)
                throw std::runtime_error("motorway edge's distance is not valid");
            // traveltime is valid
            if (e.traveltime < 0 )
                throw std::runtime_error("motorway edge's traveltime is not valid");
            // edge order is valid
            if (e.source < previousSourceId)
                throw std::runtime_error("motorway edge's edge order is wrong");
            else
                previousSourceId = e.source;
        }
    }

    void validatePedestrianEdgesAfterConnect() {

        unsigned long previousSourceId = 0;
        for (ParsingEdge e : graph->pedestrianParsingEdges) {
            // source and target ids of valid nodes
            if (e.source >= graph->nodes.size())
                throw std::runtime_error("pedestrian edge's source node is not valid");
            if (e.target >= graph->nodes.size())
                throw std::runtime_error("pedestrian edge's target node is not valid");
            // maxspeed is valid
            if (e.maxSpeed != 5)
                throw std::runtime_error("pedestrian edge's maxspeed is not valid");
            // edge order is valid
            if (e.source < previousSourceId)
                throw std::runtime_error("pedestrian edge's edge order is wrong");
            else
                previousSourceId = e.source;
        }
    }

    void validateNodesAfterConnect() {
        for (unsigned int i = 0; i < graph->nodes.size(); i++) {
            if (i !=  graph->nodes.at(i).id)
                throw std::runtime_error("node id is not valid");
        }
    }

    void validateMotorwayOffsetsAfterConnect() {
        if (graph->motorwayOffsets.size() != graph->nodes.size()) {
            throw std::runtime_error("number of motorwayOffsets does not equal number of nodes");
        }
        for (unsigned int i = 0; i < graph->motorwayOffsets.size() - 1; i++) {
            int j = graph->motorwayOffsets.at(i);
            int k = graph->motorwayOffsets.at(i + 1);
            for (; j < k; j++) {
                if (graph->motEdges.at(j).source != i)
                    throw std::runtime_error("motorway offset is wrong");
            }
        }
    }

    void validatePedestrianOffsetsAfterConnect() {
        if (graph->pedestrianOffsets.size() != graph->nodes.size()) {
            throw std::runtime_error("number of pedestrianOffsets does not equal number of nodes");
        }
        for (unsigned int i = 0; i < graph->pedestrianOffsets.size() - 1; i++) {
            int j = graph->pedestrianOffsets.at(i);
            int k = graph->pedestrianOffsets.at(i + 1);
            for (; j < k; j++) {
                if (graph->pedEdges.at(j).source != i)
                    throw std::runtime_error("pedestrian offset is wrong");
            }
        }
    }

private:
    Graph *graph;

};


#endif //FAPRA_VALIDATER_H
