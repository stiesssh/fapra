//
// Created by maumau on 15.05.19.
//

#ifndef OSMIUM_COUNT_EDGE_H
#define OSMIUM_COUNT_EDGE_H

/**
 * These edges are for motorised vehicles.
 */
class Edge {
public:
    unsigned int source; //ID in the nodes array
    unsigned int target; //ID in the nodes array
    int distance;
    int traveltime;

    Edge(unsigned int _source, unsigned int _target, int _distance, int _traveltime) :
            source(_source), target(_target), distance(_distance), traveltime(_traveltime) {}

};


#endif //OSMIUM_COUNT_EDGE_H
